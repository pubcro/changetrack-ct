#!/bin/bash
check_sudo () {
        if [ "$EUID" -ne 0 ]; then
                echo "This program must be run with root privileges"
                exit 1
        fi
        return 0
}
check_sudo
[[ -r /etc/default/changetrack ]] && . /etc/default/changetrack
for argc in $@ ; do
        case $argc in 
                auto)
                        auto=1
                        shift
                        ;;
                -n=*|--notify=*)
                        NOTIFY_TO=${argc#*=}
                        new_NOTIFY_TO=1
                        shift
                        ;;
                -f=*|--form=*)
                        NOTIFY_FROM=${argc#*=}
                        new_NOTIFY_FROM=1
                        shift
                        ;;
                testing)
                        UPGRADE="testing"
                        new_UPGRADE=1
                        shift
                        ;;
        esac
done

[[ -z $NOTIFY_TO ]] && {
        if [[ $auto != 1 ]] ; then
                echo "Type email adress for mail notification ... "
                read NOTIFY_TO
        else
                NOTIFY_TO=root
        fi
        new_NOTIFY_TO=1
}

[[ -z $NOTIFY_FROM ]] && {
        if [[ $auto != 1 ]] ; then
                echo "Type FROM email adress for mail notification ... "
                read NOTIFY_FROM
        else
                NOTIFY_FROM=root@$HOSTNAME
        fi
        new_NOTIFY_FROM=1
}

[[ -z $UPGRADE ]] && {
        UPGRADE=stable
        new_UPGRADE=1
}
echo "Configuring ChangeTrack ... "

if [[ $NOT_CONFIGURED_YET == 1 ]] ; then
        echo -n "set email notifications to $NOTIFY_TO ... "
        sed -i "s/^NOTIFY_TO=$/NOTIFY_TO=\"$NOTIFY_TO\"/" /etc/default/changetrack
        new_NOTIFY_TO=0
        echo done
        echo -n "set email notifications from $NOTIFY_FROM ... "
        sed -i "s/^NOTIFY_FROM=$/NOTIFY_FROM=\"$NOTIFY_FROM\"/" /etc/default/changetrack
        new_NOTIFY_FROM=0
        echo done
        echo -n "set PARAMS ... "
        sed -i "s/^PARAMS=$/PARAMS=\"-q -u -o \$NOTIFY_TO -f \$NOTIFY_FROM\"/" /etc/default/changetrack 
        echo done
        echo -n "activating config-files auto-track ... "
        sed -i 's/^AUTO_TRACK_ALL_CONFFILES=$/AUTO_TRACK_ALL_CONFFILES="yes"/' /etc/default/changetrack 
        echo done
        echo -n "set upgrade mode to $UPGRADE ... "
        sed -i "s/^UPGRADE=$/UPGRADE=\"$UPGRADE\"/" /etc/default/changetrack
        # remove not yet configured flag
        sed -i 's/^NOT_CONFIGURED_YET=1$/#/' /etc/default/changetrack
fi

if [[ $new_NOTIFY_TO == 1 ]] ; then
        echo -n "set email notifications to $NOTIFY_TO ... "
        sed -i "s/^NOTIFY_TO=.*$/NOTIFY_TO=\"$NOTIFY_TO\"/" /etc/default/changetrack
        echo done
fi
if [[ $new_NOTIFY_FROM == 1 ]] ; then
        echo -n "set email notifications from $NOTIFY_FROM ... "
        sed -i "s/^NOTIFY_FROM=.*$/NOTIFY_FROM=\"$NOTIFY_FROM\"/" /etc/default/changetrack
        echo done
fi
if [[ $new_UPGRADE == 1 ]] ; then
        echo -n "set upgrade mode to $UPGRADE ... "
        sed -i "s/^UPGRADE=.*$/UPGRADE=\"$UPGRADE\"/" /etc/default/changetrack
fi

echo "Perform integrated tests"
# Integrated tests
if [[ -r /etc/default/changetrack ]] ; then
        o_NOTIFY_TO=$NOTIFY_TO
        o_NOTIFY_FROM=$NOTIFY_FROM
        o_UPGRADE=$UPGRADE
        unset NOTIFY_TO NOTIFY_FROM PARAMS AUTO_TRACK_ALL_CONFFILES UPGRADE
        . /etc/default/changetrack
else
        echo "fatal error on configuration"
        exit 1
fi
# check if some parameters missing ...
[[ -z $NOTIFY_TO ]] && echo "NOTIFY_TO=\"$o_NOTIFY_TO\"" >> /etc/default/changetrack
[[ -z $NOTIFY_FROM ]] && echo "NOTIFY_FROM=\"$o_NOTIFY_FROM\"" >> /etc/default/changetrack
[[ -z $PARAMS ]] && echo "PARAMS=\"-q -u -o \$NOTIFY_TO -f \$NOTIFY_FROM\"" >> /etc/default/changetrack
[[ -z $AUTO_TRACK_ALL_CONFFILES ]] && echo "AUTO_TRACK_ALL_CONFFILES=\"yes\"" >> /etc/default/changetrack
[[ -z $UPGRADE ]] && echo "UPGRADE=\"$o_UPGRADE\"" >> /etc/default/changetrack

if [[ ! -d /etc/changetrack.d ]] ; then
        mkdir -p /etc/changetrack.d >> /dev/null 2>&1
fi

cat <<EOF > /etc/changetrack.d/auto-update.conf
/boot/grub/grub.cfg
/boot/grub/menu.lst
/etc/adduser.conf
/etc/aliases
/etc/apache2/conf-available/*
/etc/apache2/conf-enabled/*
/etc/apache2/sites-available/*
/etc/apache2/sites-enabled/*
/etc/apt/apt.conf.d/*
/etc/apt/apt-file.conf
/etc/apt/listchanges.conf
/etc/apt/preferences
/etc/apt/preferences.d/*
/etc/apt/sources.list.d/*
/etc/apt/trusted.gpg.d/*
/etc/available-updates
/etc/bash.bashrc
/etc/ca-certificates.conf
/etc/changetrack.conf
/etc/changetrack.d/*.conf
/etc/clamav/clamd.conf
/etc/clamav/freshclam.conf
/etc/cron.d/*
/etc/cron.daily/*
/etc/cron.hourly/*
/etc/cron.monthly/*
/etc/crontab
/etc/cron.weekly/*
/etc/dbus-1/session.conf
/etc/dbus-1/system.conf
/etc/dbus-1/system.d/*.conf
/etc/debconf.conf
/etc/default/*
/etc/default/varnish
/etc/deluser.conf
/etc/dhcp/dhclient.conf
/etc/discover-modprobe.conf
/etc/exim4/update-exim4.conf.conf
/etc/fail2ban/action.d/*.conf
/etc/fail2ban/fail2ban.conf
/etc/fail2ban/filter.d/*.conf
/etc/fail2ban/jail.conf
/etc/fetchmailrc
/etc/fstab
/etc/ftpusers
/etc/gai.conf
/etc/gforge/*.conf
/etc/group
/etc/gtk-2.0/im-multipress.conf
/etc/host.conf
/etc/hostname
/etc/hosts
/etc/hosts.allow
/etc/hosts.deny
/etc/idmapd.conf
/etc/init/*.conf
/etc/initramfs-tools/initramfs.conf
/etc/initramfs-tools/update-initramfs.conf
/etc/inputrc
/etc/insserv.conf
/etc/iproute2/*
/etc/iptables/*
/etc/iptables/rules.v4
/etc/iptables/rules.v6
/etc/ipvsadm_rules
/etc/iscsi/iscsid.conf
/etc/keepalived/*.conf
/etc/kernel-img.conf
/etc/ld.so.conf
/etc/ld.so.conf.d/*.conf
/etc/le/*.conf
/etc/letsencrypt/*.conf
/etc/letsencrypt/renewal/*.conf
/etc/libaudit.conf
/etc/lighttpd/conf-available/*.conf
/etc/lighttpd/conf-enabled/*.conf
/etc/logrotate.conf
/etc/lprfilter
/etc/mail.rc
/etc/mailname
/etc/mariadb/*.cnf
/etc/mariadb/conf.d/*
/etc/mke2fs.conf
/etc/modprobe.d/blacklist
/etc/modules-load.d/modules.conf
/etc/munin/*
/etc/Muttrc
/etc/myname
/etc/mysql/*.cnf
/etc/mysql/conf.d/*
/etc/nanorc
/etc/network/*
/etc/news/leafnode/config
/etc/nginx/*
/etc/nginx/*.d/*
/etc/nsswitch.conf
/etc/pam.conf
/etc/pam.d/*
/etc/passwd
/etc/pear/pear.conf
/etc/postfix/*.cf
/etc/postgresql/*.conf
/etc/postgresql/*/main/*.conf
/etc/printcap
/etc/proftpd/*.conf
/etc/quilt.quiltrc
/etc/reportbug.conf
/etc/request-key.d/id_resolver.conf
/etc/resolv.conf
/etc/role
/etc/rsyslog.conf
/etc/samba/smb.conf
/etc/security/*.conf
/etc/selinux/semanage.conf
/etc/sensors3.conf
/etc/skel/.bashrc
/etc/snort/snort.debian.conf
/etc/squid/squid.conf
/etc/ssh/sshd_config
/etc/ssl/openssl.cnf
/etc/status
/etc/sudoers
/etc/sysconfig/varnish
/etc/sysctl.conf
/etc/sysctl.d/*
/etc/systemd/*.conf
/etc/timestamp
/etc/traefik/*.toml
/etc/ucf.conf
/etc/udev/udev.conf
/etc/updatedb.conf
/etc/varnish/*.vcl
/etc/vim/vimrc
/etc/vmware-tools/tools.conf
/etc/wgetrc
/etc/xdg/user-dirs.conf
/lib/systemd/system/apache2.service.d/forking.conf
/lib/systemd/system/networking.service.d/network-pre.conf
/root/.profile
/usr/lib/postfix/*.cf
/usr/lib/ssl/openssl.cnf
/usr/lib/tmpfiles.d/*.conf
/usr/share/adduser/adduser.conf
/usr/share/debconf/debconf.conf
/usr/share/grc/conf.*
/usr/share/libc-bin/nsswitch.conf
/usr/share/logwatch/default.conf/services/*.conf
/usr/share/proftpd/templates/*.conf
/usr/share/upstart/sessions/ssh-agent.conf
/var/lib/pgsql/data/*.conf
/var/spool/cron/crontabs/*
/var/spool/postfix/etc/host.conf
/var/spool/postfix/etc/nsswitch.conf/lib/modprobe.d/aliases.conf
/var/spool/postfix/etc/resolv.conf
EOF
/opt/changetrack/bin/ct update
/opt/changetrack/bin/ct commit
