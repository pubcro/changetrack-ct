#!/bin/bash

# ENV VAR DEFINITION
export DEBIAN_FRONTEND=noninteractive
CCTREPO="https://gitlab.com/pubcro"

if [[ -f /etc/default/changetrack ]] ; then
  grep "UPGRADE=.*$" /etc/default/changetrack >> /dev/null 2>&1 && {
    . /etc/default/changetrack
    if [[ $UPGRADE == "testing" ]] ; then
      REPO='testing'
    fi
  } 
fi

# master is the default repository
REPO=${REPO:-master}
UPGRADE=${UPGRADE:-stable}

print_status() {
    echo
    echo "## $1"
    echo
}

check_sudo () {
    if [ "$EUID" -ne 0 ]; then
        echo "This program must be installed with root privileges"
        exit 1
    fi
    return 0
}


function trimString()
{
    local -r string="${1}"

    sed -e 's/^ *//g' -e 's/ *$//g' <<< "${string}"
}

function isEmptyString()
{
    local -r string="${1}"

    if [[ "$(trimString "${string}")" = '' ]] ; then
        echo 'true'
    else
        echo 'false'
    fi
}

function info()
{
    local -r message="${1}"

    echo -e "\033[1;36m${message}\033[0m" 2>&1
}

function getLastAptGetUpdate()
{
    local aptDate="$(stat -c %Y '/var/cache/apt')"
    local nowDate="$(date +'%s')"

    echo $((nowDate - aptDate))
}

function runAptGetUpdate()
{
    local updateInterval="${1}"

    local lastAptGetUpdate="$(getLastAptGetUpdate)"

    if [[ "$(isEmptyString "${updateInterval}")" = 'true' ]] ; then
        # Default To 300 second
        updateInterval="$((5 * 60))"
    fi

    if [[ "${lastAptGetUpdate}" -gt "${updateInterval}" ]] ; then
        info "apt-get update"
        exec_cmd_nobail "apt-get update -m 2>/dev/null"
        RC=$?
        if [[ $RC != 0 ]] ; then
          print_bold wargning "Could not run apt-get update ... skipping"
        fi
    else
        local lastUpdate="$(date -u -d @"${lastAptGetUpdate}" +'%-Hh %-Mm %-Ss')"

        info "\nSkip apt-get update because its last run was '${lastUpdate}' ago"
    fi
}


if test -t 1; then # if terminal
    ncolors=$(which tput > /dev/null && tput colors) # supports color
    if test -n "$ncolors" && test $ncolors -ge 8; then
        termcols=$(tput cols)
        bold="$(tput bold)"
        underline="$(tput smul)"
        standout="$(tput smso)"
        normal="$(tput sgr0)"
        black="$(tput setaf 0)"
        red="$(tput setaf 1)"
        green="$(tput setaf 2)"
        yellow="$(tput setaf 3)"
        blue="$(tput setaf 4)"
        magenta="$(tput setaf 5)"
        cyan="$(tput setaf 6)"
        white="$(tput setaf 7)"
    fi
fi

print_bold() {
    title="$1"
    text="$2"

    echo
    echo "${red}================================================================================${normal}"
    echo "${red}================================================================================${normal}"
    echo
    echo -e "  ${bold}${yellow}${title}${normal}"
    echo
    echo -en "  ${text}"
    echo
    echo "${red}================================================================================${normal}"
    echo "${red}================================================================================${normal}"
}

bail() {
    echo 'Error executing command, exiting'
    exit 1
}

exec_cmd_nobail() {
    echo "+ $1"
    bash -c "$1"
}

exec_cmd() {
    exec_cmd_nobail "$1" || bail
}

setup_countdown() {
secs=$((5 * 1))
while [ $secs -gt 0 ]; do
   echo -ne "ChangeTrack-CT will be installed in $secs\033[0K\r"
   sleep 1
   : $((secs--))
done
}

setup() {
cat << "EOL"
     ______            _____          ________                              ______                __            
    / ____/___  ____  / __(_)___ _   / ____/ /_  ____ _____  ____ ____     /_  __/________ ______/ /_____  _____
   / /   / __ \/ __ \/ /_/ / __ `/  / /   / __ \/ __ `/ __ \/ __ `/ _ \     / / / ___/ __ `/ ___/ //_/ _ \/ ___/
  / /___/ /_/ / / / / __/ / /_/ /  / /___/ / / / /_/ / / / / /_/ /  __/    / / / /  / /_/ / /__/ ,< /  __/ /    
  \____/\____/_/ /_/_/ /_/\__, /   \____/_/ /_/\__,_/_/ /_/\__, /\___/    /_/ /_/   \__,_/\___/_/|_|\___/_/     
                         /____/                           /____/                                                
EOL
setup_countdown

print_status "Installing ChangeTrack-CT ${VERSION} $UPGRADE"

if [[ "$REPO" == "testing" ]] ; then
  print_bold "Be carreful" "You will install the testing version of changetrack-ct"
fi

PRE_INSTALL_PKGS=" git rcs patch"

dpkg -s changetrack | grep "Status: install ok installed" >> /dev/null 2>&1
RC=$?
if [[ $RC == 0 ]] ; then
  print_status "Removing previous changetrack package installation"
  exec_cmd "apt-get autoremove -y changetrack"
fi

if [ ! -x /usr/bin/lsb_release ]; then
    PRE_INSTALL_PKGS="${PRE_INSTALL_PKGS} lsb-release"
fi

# we need curl or wget installed, we prefer to use curl
if [ ! -x /usr/bin/curl ] && [ ! -x /usr/bin/wget ]; then
    PRE_INSTALL_PKGS="${PRE_INSTALL_PKGS} curl"
fi

# Populating Cache
print_status "Populating apt-get cache..."
runAptGetUpdate

if [ "X${PRE_INSTALL_PKGS}" != "X" ]; then
    print_status "Installing packages required for setup:${PRE_INSTALL_PKGS}..."
    # This next command needs to be redirected to /dev/null or the script will bork
    # in some environments
    exec_cmd "apt-get install -y${PRE_INSTALL_PKGS} > /dev/null 2>&1"
fi

DISTRO=$(lsb_release -c -s)

check_alt() {
    if [ "X${DISTRO}" == "X${2}" ]; then
        echo
        echo "## You seem to be using ${1} version ${DISTRO}."
        echo "## This maps to ${3} \"${4}\"... Adjusting for you..."
        DISTRO="${4}"
    fi
}

check_alt "Kali"          "sana"     "Debian" "jessie"
check_alt "Kali"          "kali-rolling" "Debian" "jessie"
check_alt "Debian"        "stretch"  "Debian" "jessie"
check_alt "Linux Mint"    "maya"     "Ubuntu" "precise"
check_alt "Linux Mint"    "qiana"    "Ubuntu" "trusty"
check_alt "Linux Mint"    "rafaela"  "Ubuntu" "trusty"
check_alt "Linux Mint"    "rebecca"  "Ubuntu" "trusty"
check_alt "Linux Mint"    "rosa"     "Ubuntu" "trusty"
check_alt "Linux Mint"    "sarah"    "Ubuntu" "xenial"
check_alt "Linux Mint"    "serena"   "Ubuntu" "xenial"
check_alt "LMDE"          "betsy"    "Debian" "jessie"
check_alt "elementaryOS"  "luna"     "Ubuntu" "precise"
check_alt "elementaryOS"  "freya"    "Ubuntu" "trusty"
check_alt "elementaryOS"  "loki"     "Ubuntu" "xenial"
check_alt "Trisquel"      "toutatis" "Ubuntu" "precise"
check_alt "Trisquel"      "belenos"  "Ubuntu" "trusty"
check_alt "BOSS"          "anokha"   "Debian" "wheezy"
check_alt "bunsenlabs"    "bunsen-hydrogen" "Debian" "jessie"
check_alt "Tanglu"        "chromodoris" "Debian" "jessie"

if [ "X${DISTRO}" == "Xdebian" ]; then
  print_status "Unknown Debian-based distribution, checking /etc/debian_version..."
  NEWDISTRO=$([ -e /etc/debian_version ] && cut -d/ -f1 < /etc/debian_version)
  if [ "X${NEWDISTRO}" == "X" ]; then
    print_status "Could not determine distribution from /etc/debian_version..."
  else
    DISTRO=$NEWDISTRO
    print_status "Found \"${DISTRO}\" in /etc/debian_version..."
  fi
fi

print_status "Confirming \"${DISTRO}\" is supported..."

if [ -x /usr/bin/curl ]; then
    exec_cmd_nobail "curl -sLfk -o /dev/null '$CCTREPO/changetrack-ct/raw/$REPO/dists/${DISTRO}/Release'"
    RC=$?
else
    exec_cmd_nobail "wget --no-check-certificate -qO /dev/null -o /dev/null '$CCTREPO/changetrack-ct/raw/$REPO/dists/${DISTRO}/Release'"
    RC=$?
fi

if [[ $RC != 0 ]]; then
    print_status "Your distribution, identified as \"${DISTRO}\", is not currently supported, please contact ChangeTrack-CT Dev Team if you think this is incorrect or would like your distribution to be considered for support"
    exit 1
fi

# URL CHANGE PATCH - Must be reinstalled completly
if [[ -f /opt/changetrack/install ]] ; then
  grep "https://gitlab.com/pubcro/changetrack-ct" /opt/changetrack/install >> /dev/null 2>&1
  if [[ $? != 0 ]]; then
    print_status "Removing previous installation binary ..." 
    exec_cmd "rm -rf /opt/changetrack"
  fi
fi
if [[ ! -d /opt/changetrack ]] ; then 
        exec_cmd "git clone $CCTREPO/changetrack-ct.git /opt/changetrack"
        cd /opt/changetrack
        git config http.sslVerify "false"
        if [[ $REPO == "testing" ]] ; then
          exec_cmd "git checkout testing"
        fi
        cd - >> /dev/null 2>&1
else
	cd /opt/changetrack
        git config http.sslVerify "false"
        if [[ $REPO == "testing" ]] ; then
          exec_cmd "git checkout testing"
        fi
        exec_cmd "git pull"
	cd - >> /dev/null 2>&1
fi
print_status "Installing binary ..." 
exec_cmd_nobail "mkdir -p /usr/local/bin/ >> /dev/null 2>&1"
exec_cmd_nobail "rm -f /usr/local/bin/ct-update /usr/local/bin/ct /usr/local/bin/ct-upgrade /usr/local/bin/ct-configure" 
exec_cmd "ln -s /opt/changetrack/bin/ct /usr/local/bin/ct"
exec_cmd "ln -s /opt/changetrack/configure /usr/local/bin/ct-configure"
exec_cmd "ln -s /opt/changetrack/install /usr/local/bin/ct-upgrade"
print_status "Installing crontab auto-upgrade"
cat <<EOF > /etc/cron.weekly/changetrack
#!/bin/bash
MAILTO=
/usr/local/bin/ct-upgrade >> /dev/null 2>&1
/usr/local/bin/ct-upgrade >> /dev/null 2>&1
/usr/local/bin/ct-configure auto >> /dev/null 2>&1
EOF

cat <<EOCTD > /etc/cron.daily/changetrack
#!/bin/sh
## No arms, no cookies
MAILTO=
[ -x /opt/changetrack/bin/ct ] || exit 0
/opt/changetrack/bin/ct update
EOCTD

cat <<EOCTCH > /etc/cron.hourly/changetrack
#!/bin/sh
## No arms, no cookies
MAILTO=
[ -x /opt/changetrack/bin/ct ] || exit 0
/opt/changetrack/bin/ct commit
EOCTCH


chmod +x /etc/cron.weekly/changetrack /etc/cron.daily/changetrack /etc/cron.d/changetrack 2>/dev/null

if [[ ! -f /etc/default/changetrack ]] ; then
cat <<EOCTC > /etc/default/changetrack
## Default setting for changetrack.
## This file is sourced from /etc/cron.{hourly,daily}/changetrack
## and from changetrack-ct binary.
NOT_CONFIGURED_YET=1
## Standard parameters: quiet, unified diffs, and mail to NOTIFY_TO.
NOTIFY_TO=
NOTIFY_FROM=
PARAMS=

## The location of the list of conffiles
## When set to yes, all conffiles on your system is tracked for changes
## in addition to the ones you put in /etc/changetrack.d/*.conf
AUTO_TRACK_ALL_CONFFILES=
## Upgrade mode can be set to stable or testing
UPGRADE=
EOCTC
fi

#Cleaning boggon tracking file
if [[ ! -f /etc/changetrack.conf ]] ; then 
  touch /etc/changetrack.conf
fi
grep -v "*" /etc/changetrack.conf | sort -u > /tmp/$$.changetrack.conf 2> /dev/null
cat /tmp/$$.changetrack.conf > /etc/changetrack.conf
rm -f tmp/$$.changetrack.conf 
for f in $(find /var/lib/changetrack/RCS -type f 2>/dev/null | grep "*" 2>/dev/null) ; do rm "$f" 2>/dev/null; done
for f in $(find /var/lib/changetrack/ -type f 2>/dev/null | grep "*" 2>/dev/null) ; do rm "$f" 2>/dev/null; done
sed -i 's/^CONFFILES_LIST=.*$/#/' /etc/default/changetrack
if [[ -f /etc/cron.d/changetrack ]] ; then
  rm -f /etc/cron.d/changetrack
fi

if [[ $REPO == "testing" ]] ; then
  print_status "Run \`ct-configure testing' (as root) to finish installation of ChangeTrack-CT"
else
  print_status "Run \`ct-configure' (as root) to finish installation of ChangeTrack-CT"
fi

}

## Defer setup until we have the complete script
check_sudo ; setup
