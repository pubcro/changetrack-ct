# Change Log
All notable changes to this project will be documented in this file.

## [Unreleased]
### Added
- Add Advertissement to avoid editing /etc/changetrack.d/auto-update.conf
- Cleanning mechanisms database of /var/lib/changelog

### Changed

### Deprecated

### Removed

### Fixed
- Test mail-from at installation, if may fail, give a warning
- Test mail sender, if it's not installed give a warning

### Security 

### Discution
- Removing the first notification when installing

## 1.0.6 [pre-release]
## Changed
- 1.0.6 will be the next testing version

## 1.0.5.4 [latest-stable] - 2017-12-03
### Added
- support for several new config file

## 1.0.5.3 - 2017-12-03
### Changed
- update repository url

## 1.0.5.2 [latest-stable] - 2017-06-14 **CRITICAL UPDATE**
### Fixed
- changetrack send notification every hours
- fixing not functionnal cronjob

## 1.0.5.1 - 2017-06-09 
### Fixed
- changetrack send notification every hours
- fixing some minor bug in cron MAILTO definition

## 1.0.5 - 2017-05-29
### Added
- Configure and Install script will perform some integrated tests 
- Install script permit to install and configure the testing version
- -v|--version parameter for `ct` 

### Changed
- Pre-relase version will be now in the testing branch
- Auto-Upgrade can be defined in `/etc/default/changertack` with parameter `UPGRADE="stable|testing"`
- master will be the latest stable relase
- We embeded our changetrack perl code
- Use efficient cache of apt

### Fixed
- Suppress duplicates entry in /etc/changetrack.d/auto-update.conf
- Suppress boggon entry in /etc/changetrack.conf
- Suppress find error during first installation
- Prevent upgrade process to fail if changetrack is missconfigured
- Prevent apt-get populating to fail install process
- testing and stable upgrade process are fully functionnal now

### Security 
- Abord installation if changetrack is installed without changetrack-ct

### Deprecated
- CONFFILES_LIST prameter in `/etc/default/changetrack` is not used anymore


### 1.0.4 - 2017-05-22
### Added 
- Adding parameter `UPGRADE="stable"` in `/etc/default/changertack`

### Security
- Avoiding reset of `PARAMS` paramater in `/etc/default/changertack` during upgrade 


### 1.0.3 - 2017-05-19
### Added 
- ct-configure now accept --notify=|-n= as paramter for scripted installation

## 1.0.2 - 2017-05-14
### Added
- Notifications are now splitted in tow value (trackNewFile and ChangeOnFile) 

### Changed
- Default config-files database are now located in /etc/changetrack.d

### Fixed
- Some wildcard directory or sub-directory not been tracked are now tracked correctly
- Daily database update work fine now
- Most of boggon tracking entry and tracking file ares cleanned during installation process


## 1.0.1 - 2017-05-13
### Added
- Install script have been refactorized


## 1.0.0 - 2017-05-05
- First ready for production version

### Added
- Adding usage in README


## 0.0.1 - 2017-05-01
### Added
- Lab version logic and proof of concept
