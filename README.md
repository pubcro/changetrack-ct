```
     ______            _____          ________                              ______                __            
    / ____/___  ____  / __(_)___ _   / ____/ /_  ____ _____  ____ ____     /_  __/________ ______/ /_____  _____
   / /   / __ \/ __ \/ /_/ / __ `/  / /   / __ \/ __ `/ __ \/ __ `/ _ \     / / / ___/ __ `/ ___/ //_/ _ \/ ___/
  / /___/ /_/ / / / / __/ / /_/ /  / /___/ / / / /_/ / / / / /_/ /  __/    / / / /  / /_/ / /__/ ,< /  __/ /    
  \____/\____/_/ /_/_/ /_/\__, /   \____/_/ /_/\__,_/_/ /_/\__, /\___/    /_/ /_/   \__,_/\___/_/|_|\___/_/     
                         /____/                           /____/                                                
```


# ==== CHANGETRACK CT, Config tracking system for Linux ====

## Prerequisites
ChangeTrack CT is compatible with any debian-like system

## Installing
* To install the latest stable version, execute as root or with sudo : 
```
wget -qO- https://gitlab.com/pubcro/changetrack-ct/raw/master/install | bash
ct-configure
```

* To install the testing version :
```
wget -qO- https://gitlab.com/pubcro/changetrack-ct/raw/testing/install | bash
ct-configure testing
```

* To install the latest stable version with no interactive mode, execute as root or with sudo :
```
wget -qO- https://gitlab.com/pubcro/changetrack-ct/raw/master/install | bash
ct-configure -f $HOSTNAME@yoourdomain.tld -n changetrack@yoourdomain.tld
```


At the first launch, `ct-configure` will ask you for a notification email address, you can use `any valid email address`, `root` or `none`
or you can use a list of email address separate by coma ex : `notify@domain1.tld,changetrack@domain2.tld`

for integrated script, you can call `ct-configue --notify=notify@domain1.tld,changetrack@domain2.tld`. In this case `ct-configure` will not
ask you for anything

Tracking list confg file ares located in `/etc/changetrack.d/*.conf`, default config file is located in `/etc/default/changetrack`.
Don't edit `/etc/changetrack.d/auto-update.conf`. This file is auto-generated during install or update process. 
You can place in `/etc/changetrack.d/` several conf file list named with `.conf` extention who can contains a list of config files, directory, sub-directory with or without wildcard. That's no matter if the file or the directory is present on the system or not. changetract-ct will track only file really exists on you'r system.


## Upgrade :
Upgrade will be installed automactly by cron `@weekly`, you can run manual upgrade also by launching command `ct-upgrade`

## Usage
main command is `ct`

Usage : 
```
ct [command] [option]
```

### Available options :
* -h|--help
Print this help and exit
* -v|--version
Print changetrack version and exit
* add [files|dir]
Adding several files or directory to the revision tracking database
* commit
Commiting and send change
* diff [file] [rev]
Show diff between selected revision and [rev]
* log [file]
Show availables revisions for the file [file]
* print [file] [rev]
Show file content at revions [rev]
